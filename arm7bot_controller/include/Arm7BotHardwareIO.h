//
// Created by ros on 1/17/17.
//

#ifndef ARM7BOT_CONTROLLER_ARM7BOTIOINTERFACE_H
#define ARM7BOT_CONTROLLER_ARM7BOTIOINTERFACE_H

namespace arm7bot {

/**
 * Interface class for classes taking care of the communication to the hardware using an USB connection.
 *
 * The interface methods are optimized to work with the Arm7BotHW class which abstracts the 7bot hardware as hardware_interface::RobotHW to be used with
 * controllers compliant with the ROS controller_manager. The Arm7BotHW class only uses methods contained in this interface to realize communication
 * to the real hardware.
 */
class Arm7BotHardwareIO {
  
  public:
    
    /**
     *  Number of motors to read/write data from/to the robot
     */
    static const int _CFG_NUMBER_OF_MOTORS = 7;
    
    /**
     * Initializes the hardware I/O interface.
     *
     * @return TRUE if successful, else FALSE
     */
    virtual bool init() = 0;
    
    /**
     * Writes the robots servo motor positions, servo motor velocities, servo motor forces and the convergency state over all motors to the provided variables.
     *
     * If afflicted with units all values are calculated in radians!
     *
     * @param motorPositionsRad Positions of all seven servo motors in radians.
     * @param motorVelocitiesRadPerSec Velocities of all seven servo motors in radians per second. ATTENTION: Velocities are not supported by the current 7bot firmware.
     * @param motorForces The forces of all seven servo motors with the unit "1".
     * @param motorsConverge The motion state over all motors. TRUE if all motors are in motionless state, else FALSE.
     *
     * @return TRUE if motor data was obtained successfully, else FALSE.
     */
    virtual bool getMotorPostitonsVelocitiesForcesAndConvergencyRad(double (&motorPositionsRad)[_CFG_NUMBER_OF_MOTORS],
            double (&motorVelocitiesRadPerSec)[_CFG_NUMBER_OF_MOTORS],
            double (&motorForces)[_CFG_NUMBER_OF_MOTORS],
            bool &motorsConverge) = 0;
    
    /**
     * Writes new motor positions to the to the robot.
     *
     * All values must be given in radian!
     *
     * @param motorPositionsRad New motor positions to be set to the robot.
     *
     * @return TRUE if values where written to the I/O interface successfully, else FALSE.
     */
    virtual bool setMotorPositionsRadAsync(const double *motorPositionsRad) = 0;
    
    /**
     * Write fluency settings and maximum speed for each servo motor to the robot.
     *
     * The maximum speed will be enforced by the robots firmware before actually controlling the servo motors. The
     * speed limit enforced by the firmware can be set using this method but cannot be overridden by any client
     * controller.
     *
     * With fluency enabled for a servo motor the robots firmware will enforce a acceleration and deceleration phase of
     * the motor while moving to a new position resulting in a smooth and fluent motion of the robot.
     *
     * The method expects an array with the fluency setting for each motor: 0 - fluency disabled, 1 - fluency enabled.
     *
     * @note In many cases a path planning software (i.e. MoveIt) takes care for smooth motion of the robot thus the
     * fluency feature of the robots firmware is not needed.
     *
     * @param fluencyFlags Fluency flag for each motor
     * @param maxSpeedDegPerSec Maximum Speed for each motor in degree per second
     *
     * @return TRUE if fluency settings were set successfully, else FALSE.
     */
    virtual bool setFluencyAndMaxSpeedDegPerSec(const int *fluencyFlags, const int *maxSpeedDegPerSec) = 0;
};
    
}


#endif //ARM7BOT_CONTROLLER_ARM7BOTIOINTERFACE_H
