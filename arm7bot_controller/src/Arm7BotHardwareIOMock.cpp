//
// Created by ros on 3/5/17.
//

#include "Arm7BotHardwareIOMock.h"

bool arm7bot::Arm7BotHardwareIOMock::init() {
    
    ROS_INFO("Initializing serial IO mock");
    
    _currentMotorState = CFG_DEFAULT_MOTOR_STATE;
    _lastMotorState = CFG_DEFAULT_MOTOR_STATE;
    
    for (int i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
        _currentMotorPositionsDeg[i] = CFG_DEFAULT_MOTOR_POSITIONS_DEGREE[i];
        _currentFluencyFlags[i] = CFG_DEFAULT_MOTOR_FLUENCY_FLAGS[i];
        _currentMaxVelocitiesDegreesPerSecond[i] = CFG_DEFAULT_MAX_MOTOR_SPEED_DEGREE_PER_SECOND[i];
        _currentMotorForces[i] = 0;
        
        _lastMotorPositionsDeg[i] = 0;
        _lastFluencyFlags[i] = 0;
        _lastMaxVelocitiesDegreesPerSecond[i] = 0;
        _lastMotorForces[i] = 0;
    }
    
    return true;
}

bool arm7bot::Arm7BotHardwareIOMock::getMotorPostitonsVelocitiesForcesAndConvergencyRad(double (&motorPositionsRad)[_CFG_NUMBER_OF_MOTORS],
        double (&motorVelocitiesRadPerSec)[_CFG_NUMBER_OF_MOTORS],
        double (&motorForces)[_CFG_NUMBER_OF_MOTORS],
        bool &motorsConverge) {
    
    for (size_t i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
        motorPositionsRad[i] = CFG_DEG_TO_RAD_FACTOR * _currentMotorPositionsDeg[i];
        motorVelocitiesRadPerSec[i] = 0;
        motorForces[i] = _currentMotorForces[i];
    }
    motorsConverge = true;
    
    return true;
}

bool arm7bot::Arm7BotHardwareIOMock::setAllMotorState(const MotorStateEnum status) {
    _currentMotorState = status;
    return true;
}

// TODO convert to using radiant units
bool arm7bot::Arm7BotHardwareIOMock::setFluencyAndMaxSpeedDegPerSec(const int *fluencyFlags, const int *velocitiesDegreesPerSecond) {
    for (size_t i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
        _currentFluencyFlags[i] = fluencyFlags[i];
        _currentMaxVelocitiesDegreesPerSecond[i] = velocitiesDegreesPerSecond[i];
    }
    return true;
}

bool arm7bot::Arm7BotHardwareIOMock::readMotorState(MotorStateEnum &motorState) {
    motorState = _currentMotorState;
    return true;
}

bool arm7bot::Arm7BotHardwareIOMock::setMotorPositionsRadAsync(const double *motorPositionsRad) {
    
    for (size_t i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
        _lastMotorPositionsDeg[i] = _currentMotorPositionsDeg[i];
        _currentMotorPositionsDeg[i] = (float) (CFG_RAD_TO_DEG_FACTOR * motorPositionsRad[i]);
    }
    
    if (_currentMotorPositionsDeg != _lastMotorPositionsDeg) {
        ROS_DEBUG("MOCK: Changed motor positions to %s, in Deg: %s",
                ArrayUtil::arrayToString<double>(motorPositionsRad, CFG_NUMBER_OF_MOTORS).c_str(),
                ArrayUtil::arrayToString<double>(motorPositionsRad, CFG_NUMBER_OF_MOTORS,
                        CFG_RAD_TO_DEG_FACTOR).c_str());
    }
}