//
// Created by ros on 3/3/17.
//

#ifndef ARM7BOT_EXAMPLE_ARM7BOTARM_H
#define ARM7BOT_EXAMPLE_ARM7BOTARM_H

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <control_msgs/FollowJointTrajectoryAction.h>

typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> FollowJointTrajectoryClient;

class Arm7BotArm {
  
  FollowJointTrajectoryClient *_followJointTrajectoryClient;
  
  control_msgs::FollowJointTrajectoryGoal _currentGoal;
  unsigned long _nrOfPoses = 0;
 
 public:
  
  Arm7BotArm() {
    
    _followJointTrajectoryClient = new FollowJointTrajectoryClient("joint_trajectory_controller/follow_joint_trajectory", true);
    
    // wait for the controller to come up
    while (!_followJointTrajectoryClient->waitForServer(ros::Duration(5.0))) {
      ROS_INFO("Waiting for the JointTrajectoryController to come up...");
    }
  }
  
  ~Arm7BotArm() {
    delete _followJointTrajectoryClient;
  }
  
  void newGoal() {
    control_msgs::FollowJointTrajectoryGoal goal;
  
    goal.trajectory.joint_names.push_back("m0_base_link_to_bogie");
    goal.trajectory.joint_names.push_back("m1_bogie_to_upper_arm");
    goal.trajectory.joint_names.push_back("m2_ellbow_virtual_to_ellbow");
    goal.trajectory.joint_names.push_back("m3_ellbow_to_forearm");
    goal.trajectory.joint_names.push_back("m4_forearm_to_wrist");
    goal.trajectory.joint_names.push_back("m5_wrist_to_tool_holder_virtual");
  
    _currentGoal = goal;
    _nrOfPoses = 0;
  }
  
  void executeGoal() {
    _currentGoal.trajectory.header.stamp = ros::Time::now();
    _followJointTrajectoryClient->sendGoal(_currentGoal);
  }
  
  void addPose(const double positions[6], const ros::Duration time_from_start) {
    _currentGoal.trajectory.points.resize(_nrOfPoses + 1);
    
    _currentGoal.trajectory.points[_nrOfPoses].positions.resize(6);
    _currentGoal.trajectory.points[_nrOfPoses].velocities.resize(6);
    for (size_t j = 0; j < 6; j++) {
      _currentGoal.trajectory.points[_nrOfPoses].positions[j] = positions[j];
      _currentGoal.trajectory.points[_nrOfPoses].velocities[j] = 0.0;
    }
    _currentGoal.trajectory.points[_nrOfPoses].time_from_start = time_from_start;
    
    _nrOfPoses++;
  }
  
  actionlib::SimpleClientGoalState getState() {
    return _followJointTrajectoryClient->getState();
  }
    
    void waitForGoalToFinish() {
      ros::Rate rate(ros::Duration(1.0));
      while (!getState().isDone() && ros::ok()) {
        ROS_INFO("Waiting for trajectory to finish...");
        rate.sleep();
      }
    }
};


#endif //ARM7BOT_EXAMPLE_ARM7BOTARM_H
