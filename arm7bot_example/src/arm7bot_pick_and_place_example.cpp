//
// Created by max on 02.03.17.
//

#include <ros/ros.h>
#include "Arm7BotArm.h"
#include "Arm7BotGripper.h"

// the positions:
double pos_home[6] = {1.57, 2.01, 1.13, 1.57, 1.57, 1.57};
double pos_left[6] = {0, 2.01, 1.13, 1.57, 1.57, 1.57};
double pos_right[6] = {3.14, 2.01, 1.13, 1.57, 1.57, 1.57};
double pos_pickup[6] = {0.91, 1.38, 1.98, 1.62, 2.20, 1.57};
double pos_lift_1[6] = {0.91, 1.48, 1.86, 1.62, 2.36, 1.57};
double pos_lift_2[6] = {3.14, 1.48, 1.86, 1.62, 2.36, 0.00};
double pos_place[6] = {3.14, 1.38, 1.98, 1.62, 2.20, 0.00};

int main(int argc, char **argv) {
    
    ros::init(argc, argv, "arm7bot_pick_and_place_example");
    
    Arm7BotArm arm7BotRobotArm;
    Arm7BotGripper arm7BotGripper;
    double secondsFromStart = 0.5;
    
    ROS_INFO("Move gripper to gripping pose");
    arm7BotRobotArm.newGoal();
    arm7BotRobotArm.addPose(pos_home, ros::Duration(secondsFromStart));
    arm7BotRobotArm.addPose(pos_lift_1, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_pickup, ros::Duration(secondsFromStart += 1.0));
    
    arm7BotRobotArm.executeGoal();
    arm7BotRobotArm.waitForGoalToFinish();
    
    ROS_INFO("Grasp object");
    arm7BotGripper.actuateGripper(0.53);
    arm7BotGripper.waitForGoalToFinish();
    
    ROS_INFO("Lift object and place it at release target");
    arm7BotRobotArm.newGoal();
    secondsFromStart = 0.5;
    arm7BotRobotArm.addPose(pos_pickup, ros::Duration(secondsFromStart += 0.0));
    arm7BotRobotArm.addPose(pos_lift_1, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_lift_2, ros::Duration(secondsFromStart += 2.0));
    arm7BotRobotArm.addPose(pos_place, ros::Duration(secondsFromStart += 1.0));
    
    arm7BotRobotArm.executeGoal();
    arm7BotRobotArm.waitForGoalToFinish();
    
    ROS_INFO("Release object");
    arm7BotGripper.actuateGripper(1.39);
    arm7BotGripper.waitForGoalToFinish();
    
    ROS_INFO("Move arm back to home position");
    arm7BotRobotArm.newGoal();
    secondsFromStart = 0.5;
    arm7BotRobotArm.addPose(pos_place, ros::Duration(secondsFromStart += 0.0));
    arm7BotRobotArm.addPose(pos_lift_2, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_home, ros::Duration(secondsFromStart += 2.0));
    
    arm7BotRobotArm.executeGoal();
    arm7BotRobotArm.waitForGoalToFinish();
}