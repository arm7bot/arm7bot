//
// Created by max on 02.03.17.
//

#include <ros/ros.h>
#include "Arm7BotArm.h"

// the positions:
double pos_home[6] = {1.57, 2.01, 1.13, 1.57, 1.57, 1.57};
double pos_left[6] = {0, 2.01, 1.13, 1.57, 1.57, 1.57};
double pos_right[6] = {3.14, 2.01, 1.13, 1.57, 1.57, 1.57};
double pos_pickup[6] = {0.91, 1.07, 2.17, 1.62, 2.01, 1.57};
double pos_lift_1[6] = {0.91, 1.60, 1.73, 1.62, 2.48, 1.57};
double pos_lift_2[6] = {3.14, 1.60, 1.73, 1.62, 2.48, 0.00};
double pos_place[6] = {3.14, 0.66, 1.88, 1.57, 2.39, 0.00};

int main(int argc, char **argv) {
    
    ros::init(argc, argv, "arm7bot_follow_joint_trajectory_example");
    
    Arm7BotArm arm7BotRobotArm;
    
    arm7BotRobotArm.newGoal();
    double secondsFromStart = 0.0;
    
    arm7BotRobotArm.addPose(pos_home, ros::Duration(secondsFromStart));
    arm7BotRobotArm.addPose(pos_lift_1, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_pickup, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_pickup, ros::Duration(secondsFromStart += 0.5));
    arm7BotRobotArm.addPose(pos_lift_1, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_lift_2, ros::Duration(secondsFromStart += 2.0));
    arm7BotRobotArm.addPose(pos_place, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_place, ros::Duration(secondsFromStart += 0.5));
    arm7BotRobotArm.addPose(pos_lift_2, ros::Duration(secondsFromStart += 1.0));
    arm7BotRobotArm.addPose(pos_home, ros::Duration(secondsFromStart += 2.0));
    
    arm7BotRobotArm.executeGoal();
    
    ros::Rate rate(ros::Duration(1.0));
    while (!arm7BotRobotArm.getState().isDone() && ros::ok()) {
        ROS_INFO("Waiting for trajectory to finish...");
        rate.sleep();
    }
}