//
// Created by ros on 1/12/17.
//

#include <ros/ros.h>
#include <controller_manager/controller_manager.h>

#include "Arm7BotHW.h"

int main(int argc, char **argv) {
    
    ros::init(argc, argv, "arm7bot_controller");
    ros::NodeHandle nodeHandle;
    
    // set logger_level to DEBUG if desired
    if (nodeHandle.param("debug", false) && ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) {
        ros::console::notifyLoggerLevelsChanged();
    }
    
    arm7bot::Arm7BotHW robot(nodeHandle);
    
    controller_manager::ControllerManager cm(&robot);
    
    ROS_INFO("ControllerManager initialized");
    
    // necessary. otherwise controllers cannot be started
    ros::AsyncSpinner spinner(1);
    spinner.start();
    
    // control loop
    ros::Time prev_time = ros::Time::now();
    double ctrl_loop_rate = nodeHandle.param("ctrl_loop_rate", 33.0);
    ros::Rate rate(ctrl_loop_rate);
    
    ROS_INFO("Running 7bot control loop at %f Hz", ctrl_loop_rate);
    
    while (ros::ok()) {
        
        const ros::Time time = ros::Time::now();
        const ros::Duration period = time - prev_time;
        prev_time = time;
        
        robot.read();
        
        cm.update(time, period);
        
        robot.write();
        
        rate.sleep();
    }
    
    ROS_INFO("Exiting 7Bot node!");
    return 0;
}