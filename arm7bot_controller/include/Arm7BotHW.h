//
// Created by ros on 1/12/17.
//

#ifndef ARM7BOT_CONTROLLER_ARM7BOTHW_H
#define ARM7BOT_CONTROLLER_ARM7BOTHW_H

#include <hardware_interface/robot_hw.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>

#include "Arm7BotHardwareIOMock.h"

namespace arm7bot {

class Arm7BotHW : public hardware_interface::RobotHW {
  
  public:
    
    Arm7BotHW(ros::NodeHandle &nodeHandle);
    
    virtual ~Arm7BotHW();
    
    void read();
    
    void write();
  
  private:
    
    // robot state in local memory
    double _jointPositionsRad[CFG_NUMBER_OF_MOTORS];
    double _jointVelocitiesRadPerSec[CFG_NUMBER_OF_MOTORS];
    double _jointEfforts[CFG_NUMBER_OF_MOTORS];
    double _jointPositionsCmdRad[CFG_NUMBER_OF_MOTORS];
    bool _motorsConverge;
    
    // hardware interface
    hardware_interface::JointStateInterface _jointStateInterface;
    hardware_interface::PositionJointInterface _positionJointInterface;
    
    // Serial Communication with the robot
    arm7bot::Arm7BotHardwareIO *_hardwareIOInterface;
    
};
    
}

#endif //ARM7BOT_CONTROLLER_ARM7BOTHW_H
