//
// Created by ros on 11/4/16.
//

#ifndef ARM7BOT_MOTORSTATEENUM_H
#define ARM7BOT_MOTORSTATEENUM_H

namespace arm7bot {

enum MotorStateEnum {
    FORCELESS = 0,
    NORMAL_SERVO = 1,
    PROTECTION = 2
};
    
}

#endif //ARM7BOT_MOTORSTATEENUM_H
