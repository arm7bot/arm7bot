//
// Created by ros on 3/3/17.
//

#ifndef ARM7BOT_EXAMPLE_ARM7BOTGRIPPER_H
#define ARM7BOT_EXAMPLE_ARM7BOTGRIPPER_H

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <control_msgs/GripperCommandAction.h>

typedef actionlib::SimpleActionClient<control_msgs::GripperCommandAction> GripperCommandClient;

class Arm7BotGripper {
    
    GripperCommandClient *_gripperCommandClient;
  
  public:
    
    Arm7BotGripper() {
        _gripperCommandClient = new GripperCommandClient("gripper_action_controller/gripper_cmd", true);
        
        // wait for the controller to come up
        while (!_gripperCommandClient->waitForServer(ros::Duration(5.0))) {
            ROS_INFO("Waiting for the GripperActionController to come up...");
        }
    }
    
    ~Arm7BotGripper() {
        delete _gripperCommandClient;
    }
    
    control_msgs::GripperCommandGoal newGoal() {
        control_msgs::GripperCommandGoal goal;
        return goal;
    }
    
    void actuateGripper(const double value) {
        control_msgs::GripperCommandGoal goal = newGoal();
        goal.command.position = value;
        _gripperCommandClient->sendGoal(goal);
    }
    
    actionlib::SimpleClientGoalState getState() {
        return _gripperCommandClient->getState();
    }
    
    void waitForGoalToFinish() {
        ros::Rate rate(ros::Duration(1.0));
        while (!getState().isDone() && ros::ok()) {
            ROS_INFO("Waiting for gripper to finish...");
            rate.sleep();
        }
    }
};


#endif //ARM7BOT_EXAMPLE_ARM7BOTGRIPPER_H
