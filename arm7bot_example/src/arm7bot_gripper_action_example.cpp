//
// Created by max on 02.03.17.
//

#include <ros/ros.h>
#include "Arm7BotGripper.h"

int main(int argc, char **argv) {
    
    ros::init(argc, argv, "arm7bot_gripper_action_example");
    
    Arm7BotGripper arm7BotGripper;
    
    arm7BotGripper.actuateGripper(0);
    
    usleep(500000);
    
    arm7BotGripper.actuateGripper(1.3);
}