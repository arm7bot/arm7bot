//
// Created by max on 14.10.16.
//

#ifndef ROS7BOT_ARRAYUTIL_H
#define ROS7BOT_ARRAYUTIL_H

#include <string>
#include <sstream>

namespace arm7bot {

class ArrayUtil {
  
  public:
    
    template<typename ArrayType>
    static std::string arrayToString(const ArrayType *array, const size_t arraySize) {
        std::stringstream ss;
        ss << "[";
        
        for (int i = 0; i < arraySize; i++) {
            ss << array[i];
            if (i < arraySize - 1) {
                ss << ", ";
            }
        }
        
        ss << "]";
        return ss.str();
    }
    
    template<typename ArrayType>
    static std::string arrayToString(const ArrayType *array, const size_t arraySize, const ArrayType factor) {
        std::stringstream ss;
        ss << "[";
        
        for (int i = 0; i < arraySize; i++) {
            ss << array[i] * factor;
            if (i < arraySize - 1) {
                ss << ", ";
            }
        }
        
        ss << "]";
        return ss.str();
    }
    
    template<typename ArrayType>
    static bool equals(const ArrayType *left, const ArrayType *right, const size_t arraySize) {
        
        for (int i = 0; i < arraySize; i++) {
            if (left[i] != right[i]) {
                return false;
            }
        }
        
        return true;
    }
    
};
    
}

#endif //ROS7BOT_ARRAYUTIL_H
