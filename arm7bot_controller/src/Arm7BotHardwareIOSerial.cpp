//
// Created by max on 08.10.16.
//

#include <ros/ros.h>

#include "Arm7BotHardwareIOSerial.h"
#include "ArrayUtil.h"


arm7bot::Arm7BotHardwareIOSerial::Arm7BotHardwareIOSerial(ros::NodeHandle &nodeHandle) : _serial_port_name(
        nodeHandle.param("serial_port_name", std::string("/dev/ttyACM0"))) {
    // nothing to do here
}

arm7bot::Arm7BotHardwareIOSerial::~Arm7BotHardwareIOSerial() {
    delete _serialStreamPtr;
}

bool arm7bot::Arm7BotHardwareIOSerial::init() {
    
    ROS_INFO("Initializing serial stream on port %s", _serial_port_name.c_str());
    
    _serialStreamPtr = new LibSerial::SerialStream(_serial_port_name, CFG_SERIAL_BAUD_RATE, CFG_SERIAL_CHAR_SIZE,
            CFG_SERIAL_PARITY, CFG_SERIAL_NUMBER_OF_STOP_BITS, CFG_SERIAL_FLOW_CONTROL);
    
    if (isReady()) {
        ROS_INFO("Successfully opened serial port %s", _serial_port_name.c_str());
        return true;
    } else {
        ROS_ERROR("Failed to open serial port %s", _serial_port_name.c_str());
    }
    
    return false;
}

bool arm7bot::Arm7BotHardwareIOSerial::isReady() {
    
    if (!_serialStreamPtr) {
        ROS_ERROR("Serial stream is not initialized");
        return false;
    }
    
    if (!_serialStreamPtr->IsOpen()) {
        ROS_ERROR("Serial port is not open: %s", _serial_port_name.c_str());
        return false;
    }
    
    if (!_serialStreamPtr->good()) {
        ROS_ERROR("Serial stream has failed / is in error state");
        return false;
    }
    
    return true;
}

bool arm7bot::Arm7BotHardwareIOSerial::setAllMotorState(const MotorStateEnum status) {
    
    ROS_DEBUG_STREAM("Setting motor state to " << status);
    
    if (isReady()) {
        
        writeByte(Arm7BotInstructionEnum::START_FLAG);
        writeByte(Arm7BotInstructionEnum::SET_MOTOR_STATE);
        writeByte(status);
        
        MotorStateEnum motorState7bot;
        
        while (!readMotorState(motorState7bot)) {
        }  // Try to read motor state until successful
        
        if (motorState7bot == status) {
            ROS_DEBUG_STREAM("Successfully set motor state to " << status);
            return true;
        } else {
            ROS_ERROR_STREAM("Failed to set motor state to " << status);
        }
    }
    
    return false;
}

// TODO: Convert to using radian units
bool arm7bot::Arm7BotHardwareIOSerial::setFluencyAndMaxSpeedDegPerSec(const int *fluencyFlags,
        const int *velocitiesDegreesPerSecond) {
    
    ROS_DEBUG("Setting fluency & speed");
    ROS_DEBUG("fluencyFlags = %s", ArrayUtil::arrayToString<int>(fluencyFlags, CFG_NUMBER_OF_MOTORS).c_str());
    ROS_DEBUG("velocitiesDegreesPerSecond = %s",
            ArrayUtil::arrayToString<int>(velocitiesDegreesPerSecond, CFG_NUMBER_OF_MOTORS).c_str());
    
    if (isReady()) {
        
        writeByte(Arm7BotInstructionEnum::START_FLAG);
        writeByte(Arm7BotInstructionEnum::SET_FLUENCY_AND_SPEED);
        
        for (int i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
            
            int motorByte = velocitiesDegreesPerSecond[i] / 10;
            
            if (fluencyFlags[i] == 1) {
                motorByte += 64;
            }
            
            writeByte(motorByte);
        }
        
        return true;
    }
    
    return false;
}

bool arm7bot::Arm7BotHardwareIOSerial::readMotorState(arm7bot::MotorStateEnum &motorState) {
    
    ROS_INFO("Trying read motor state from serial port %s", _serial_port_name.c_str());
    
    if (isReady()) {
        
        writeByte(Arm7BotInstructionEnum::START_FLAG);
        writeByte(Arm7BotInstructionEnum::READ_MOTOR_STATE);
        
        if (waitForInstruction(Arm7BotInstructionEnum::READ_MOTOR_STATE)) {
            
            motorState = (MotorStateEnum) getNextByte();
            
            ROS_INFO("Successfully read motor state");
            ROS_DEBUG_STREAM("motorState = " << motorState);
            
            return true;
        }
        
    }
    
    ROS_ERROR("Failed to read motor state");
    
    return false;
}

// TODO: convert to using radian units
bool arm7bot::Arm7BotHardwareIOSerial::readFluencyAndSpeed(int *fluencyFlags, int *velocitiesDegreesPerSecond) {
    
    ROS_DEBUG("Reading fluency and speed");
    
    if (isReady()) {
        
        writeByte(Arm7BotInstructionEnum::START_FLAG);
        writeByte(Arm7BotInstructionEnum::READ_FLUENCY_AND_SPEED);
        
        if (waitForInstruction(Arm7BotInstructionEnum::READ_FLUENCY_AND_SPEED, 0)) {
            
            for (int i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
                
                int motorByte = getNextByte();
                
                fluencyFlags[i] = motorByte - 64 > 0 ? 1 : 0;
                velocitiesDegreesPerSecond[i] = (motorByte & 0x1F) * 10;
            }
            
            ROS_DEBUG("Successfully read fluency and speed");
            ROS_DEBUG("fluencyFlags = %s", ArrayUtil::arrayToString<int>(fluencyFlags, CFG_NUMBER_OF_MOTORS).c_str());
            ROS_DEBUG("velocitiesDegreesPerSecond = %s",
                    ArrayUtil::arrayToString<int>(velocitiesDegreesPerSecond, CFG_NUMBER_OF_MOTORS).c_str());
            
            return true;
        }
    }
    
    ROS_ERROR("Failed to read fluency and speed");
    
    return false;
}


int arm7bot::Arm7BotHardwareIOSerial::getNextByte() {
    
    while (_serialStreamPtr->rdbuf()->in_avail() == 0) {
        // wait until a byte is available
    }
    
    int nextByte = _serialStreamPtr->get();
    
    return nextByte;
}

bool arm7bot::Arm7BotHardwareIOSerial::waitForInstruction(const arm7bot::Arm7BotInstructionEnum instruction,
        const double timeout_sec) {
    
    double startTimeSec = ros::Time::now().toSec();
    
    while (timeout_sec == 0 || ros::Time::now().toSec() - startTimeSec < timeout_sec) {
        
        if (waitForStartFlag(timeout_sec / 10) && getNextByte() == instruction) {
            return true;
        }
        
    }
    
    return false;
}

bool arm7bot::Arm7BotHardwareIOSerial::waitForStartFlag(const double timeout_sec) {
    
    double startTimeSec = ros::Time::now().toSec();
    
    while (timeout_sec == 0 || ros::Time::now().toSec() - startTimeSec < timeout_sec) {
        
        if (!byteAvailable()) {
            continue;
        }
        
        if (getNextByte() == Arm7BotInstructionEnum::START_FLAG) {
            return true;
        }
        
    }
    
    return false;
}

bool arm7bot::Arm7BotHardwareIOSerial::byteAvailable() const {
    return _serialStreamPtr->rdbuf()->in_avail() > 0;
}

void arm7bot::Arm7BotHardwareIOSerial::writeByte(const int byte) {
    
    char outBuf[] = {(char) byte};
    
    if (isReady()) {
        _serialStreamPtr->write(outBuf, 1);
    } else {
        ROS_ERROR("Failed to send byte: %X", byte);
    }
}

bool arm7bot::Arm7BotHardwareIOSerial::setMotorPositionsRadAsync(const double *motorPositionsRad) {
    
    if (isReady()) {
        
        writeByte(Arm7BotInstructionEnum::START_FLAG);
        writeByte(Arm7BotInstructionEnum::SET_MOTOR_POSITIONS);
        
        for (int i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
            
            int motorPosition1000th = convertRadTo1000th(motorPositionsRad[i]);
            
            int lowByte = motorPosition1000th & 0x7F;
            int highByte = motorPosition1000th >> 7;
            
            writeByte(highByte);
            writeByte(lowByte);
        }
        
        return true;
    }
    
    ROS_ERROR("Failed to set motor positions");
    
    return false;
}

int arm7bot::Arm7BotHardwareIOSerial::convertRadTo1000th(const double radian) {
    return ROUND_TO_INT(CFG_RAD_TO_1000TH_FACTOR * radian);
}

bool
arm7bot::Arm7BotHardwareIOSerial::getMotorPostitonsVelocitiesForcesAndConvergencyRad(double (&motorPositionsRad)[_CFG_NUMBER_OF_MOTORS],
        double (&motorVelocitiesRadPerSec)[_CFG_NUMBER_OF_MOTORS],
        double (&motorForces)[_CFG_NUMBER_OF_MOTORS],
        bool &motorsConverge) {
    
    return isReady() && readMotorPositionsRad(motorPositionsRad, motorVelocitiesRadPerSec, motorForces, motorsConverge);
    
}

bool arm7bot::Arm7BotHardwareIOSerial::readMotorPositionsRad(double (&motorPositionsRad)[_CFG_NUMBER_OF_MOTORS],
        double (&motorVelocitiesRadPerSec)[_CFG_NUMBER_OF_MOTORS],
        double (&motorForces)[_CFG_NUMBER_OF_MOTORS],
        bool &motorsConverge) {
    
    if (waitForInstruction(Arm7BotInstructionEnum::READ_MOTOR_POSITIONS)) {
        
        for (int i = 0; i < _CFG_NUMBER_OF_MOTORS; i++) {
            
            const int highByte = getNextByte();
            const int lowByte = getNextByte();
            
            const int motorForceValue = (highByte >> 3) & 0x07;
            const int motorForceSign = (highByte >> 6) == 1 ? -1 : 1;
            
            motorPositionsRad[i] = this->convert1000thToRad((lowByte & 0x7F) + (highByte & 0x07) * 128);
            motorVelocitiesRadPerSec[i] = 0.0;
            motorForces[i] = motorForceValue * motorForceSign;
        }
        
        motorsConverge = getNextByte() == 1;
        
        return true;
    }
    
    ROS_WARN("Reading positions failed!");
    
    return false;
}

const double arm7bot::Arm7BotHardwareIOSerial::convert1000thToRad(const int n1000th) {
    return n1000th / CFG_RAD_TO_1000TH_FACTOR;
}
