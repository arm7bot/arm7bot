//
// Created by max on 11.10.16.
//

#ifndef ROS7BOT_ARM7BOTCONFG_H
#define ROS7BOT_ARM7BOTCONFG_H

#include <string>
#include <SerialStream.h>
#include <ctime>

#include "Arm7BotHardwareIOSerial.h"
#include "MotorStateEnum.h"

// Debug prints for development process.
#define DEBUG_BUILD
#ifdef DEBUG_BUILD
#define DEBUG(x) std::cout << "[     ] " << \
"[" << std::setprecision(9) << std::fixed << ((double) std::chrono::system_clock::now().time_since_epoch().count() / 1000000000) << "]: " << \
__FILE__ << ":" << __LINE__ << " " << x << std::endl
#else
#define DEBUG(x) do { } while(0)
#endif // DEBUG_BUILD

// Round any numeric value to the closest integer number
#define ROUND_TO_INT(f) ((int)(f >= 0.0 ? (f + 0.5) : (f - 0.5)))

namespace arm7bot {

static const double CFG_ROBOT_UPDATE_RATE_HZ = (1000. / 33.); // siehe Arm7Bot.cpp um Zeile 973

/** The total number of destict controllable actuators (motors) on the robot. Includes joint actuators and gripper actuators (servo or vacuum pump). */
static const int CFG_NUMBER_OF_MOTORS = 7;

/** Represents the number of actual controllable joints of the robot without gripper actuators etc. */
static const int CFG_NUMBER_OF_JOINTS = 6;

static const std::map<int, std::string> CFG_MOTOR_INDEX_TO_JOINT_NAME_MAP = {
        {0, "m0_base_link_to_bogie"},
        {1, "m1_bogie_to_upper_arm"},
        {2, "m2_ellbow_virtual_to_ellbow"},
        {3, "m3_ellbow_to_forearm"},
        {4, "m4_forearm_to_wrist"},
        {5, "m5_wrist_to_tool_holder_virtual"},
        // tool_holder_virtual_to_gripper_base is fixed -> not controllable
        {6, "m6_tool_actuator"}};

static const std::vector<std::string> CFG_JOINT_NAMES = {
        "m0_base_link_to_bogie",
        "m1_bogie_to_upper_arm",
        "m2_ellbow_virtual_to_ellbow",
        "m3_ellbow_to_forearm",
        "m4_forearm_to_wrist",
        "m5_wrist_to_tool_holder_virtual",
        // tool_holder_virtual_to_gripper_base is fixed -> not controllable
        "m6_tool_actuator"
};

// 7th value is gripper servo or pump on/off (<30 == on)
static const float CFG_DEFAULT_MOTOR_POSITIONS_DEGREE[] = {90, 115, 65, 90, 90, 90, 75};
static const int CFG_DEFAULT_MAX_MOTOR_SPEED_DEGREE_PER_SECOND[] = {70, 70, 70, 120, 120, 120, 250};
static const int CFG_DEFAULT_MOTOR_FLUENCY_FLAGS[] = {0, 0, 0, 0, 0, 0, 0};
static const MotorStateEnum CFG_DEFAULT_MOTOR_STATE = MotorStateEnum::NORMAL_SERVO;

// Serial connection settings
static const LibSerial::SerialStreamBuf::BaudRateEnum CFG_SERIAL_BAUD_RATE = LibSerial::SerialStreamBuf::BAUD_57600;
static const LibSerial::SerialStreamBuf::CharSizeEnum CFG_SERIAL_CHAR_SIZE = LibSerial::SerialStreamBuf::CHAR_SIZE_8;
static const LibSerial::SerialStreamBuf::ParityEnum CFG_SERIAL_PARITY = LibSerial::SerialStreamBuf::PARITY_NONE;
static const int CFG_SERIAL_NUMBER_OF_STOP_BITS = 1;
static const LibSerial::SerialStreamBuf::FlowControlEnum CFG_SERIAL_FLOW_CONTROL = LibSerial::SerialStreamBuf::FLOW_CONTROL_NONE;

static const double CFG_DEG_TO_RAD_FACTOR = (2 * M_PI) / 360.0;
static const double CFG_RAD_TO_DEG_FACTOR = 1 / CFG_DEG_TO_RAD_FACTOR;

static const double CFG_DEG_TO_1000TH_FACTOR = 1000. / 180.;
static const double CFG_RAD_TO_1000TH_FACTOR = 1000.0 / M_PI;
    
}

#endif //ROS7BOT_ARM7BOTCONFG_H
