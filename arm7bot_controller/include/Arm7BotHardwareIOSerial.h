//
// Created by max on 08.10.16.
//

#ifndef ROS7BOT_ARM7BOTSERIALCOMMUNICATION_H
#define ROS7BOT_ARM7BOTSERIALCOMMUNICATION_H

#include <SerialStream.h>
#include <boost/shared_ptr.hpp>

#include "Arm7BotConfg.h"
#include "MotorStateEnum.h"
#include "Arm7BotInstructionEnum.h"
#include "Arm7BotHardwareIO.h"


namespace arm7bot {

/**
 * Covers the communication to the 7Bot robotic arm using a serial link via the programming interface of the Ardunino
 * Due board. The class provides methods for sending an receiving data to and from 7Bot.
 *
 * Communication protocol:
 * - Protocol Type: UART
 * - Baud rate: 115200
 * - Characteristic: 8 data bits, no parity bit, 1 stop bit
 *
 * Communication format (Byte: Range):
 * - Byte 1 (Beginning flag): 0xFE
 * - Byte 2 (Instruction type): 0xF1 - 0xFC
 * - Byte 3+ (Data bytes): 0 - 127 (0x00 - 0x7F)
 *
 * Number of servo motors at the robotic arm:
 * The Number of servo motors at the robotic arm is supposed to be 7 (see constant 'NUMBER_OF_SERVOS'). This number also
 * includes the motor controlling the gripper.
 *
 * For more information on the communication protocol provided by the 'softwareSystem' Arduino software see:
 * https://www.dropbox.com/s/pytci253ql5lmey/Communication%20Instruction%20%28v1.0.1%29.pdf
 */
class Arm7BotHardwareIOSerial : public arm7bot::Arm7BotHardwareIO {
  
  public:
    
    /**
     * Instantiates an object for serial communication with the 7Bot
     *
     * @param nodeHandle The name of the serial port by which the robot is connected to the host pc
     *
     * @return Object for serial communication
     */
    Arm7BotHardwareIOSerial(ros::NodeHandle &nodeHandle);
    
    /**
     * Destructor
     */
    virtual ~Arm7BotHardwareIOSerial();
    
    /**
     * Initializes the serial interface.
     *
     * @return TRUE if connection was established successful, FALSE else.
     */
    virtual bool init();
    
    /**
     * Sets an operational state to all servo motors.
     *
     * @param status Motor status. See MotorStateEnum
     *
     * @return TRUE if motor state was set successfully, FALSE else
     */
    virtual bool setAllMotorState(const MotorStateEnum status);
    
    /**
     * Sets fluency and (max) speed to the robots servo motors.
     *
     * The method requires two arrays at the size of the number of servo motors available an the robotic arm. Both of the
     * arrays are expected to be ordered according to the motor numbering shown in the communication protocol documentation
     * (see class description for link).
     *
     * If the robotic arm is operated without the gripper installed, the last value of each array is beeing ignored. It
     * still has to be given though.
     *
     * Fluency:
     * Fluency can be enabled or diabled using the int values '1' (enabled) or '0' (disabled) at the desired position of
     * the 'fluencyFlags' array. Enabling fluency causes the motor to have an acceleration phase at the begin and a
     * decelaration phase at the end of its motion.
     *
     * (Maximum) motor speed:
     * Each motor can be set to an individual motion speed. If the fluency flag is set for the motor, the assigned speed
     * is the maximum speed the motor will reach during its motion. The unit is degrees per second.
     *
     * @param fluencyFlags Array of fluency flags
     * @param velocitiesDegreesPerSecond Array of (max) motor velocities in degrees per second
     *
     * @return TRUE if fluency and (max) motor velocities were set successfully, FALSE else
     */
    virtual bool setFluencyAndMaxSpeedDegPerSec(const int *fluencyFlags, const int *velocitiesDegreesPerSecond);
    
    /**
     * Reads the active motor state from the Robot.
     *
     * Reading from the robot may fail sporadely. The operation can be repeated in such case.
     *
     * @param motorState Variable where the read motor state will be written to
     *
     * @return TRUE if motor state was read successfully, FALSE else.
     */
    virtual bool readMotorState(MotorStateEnum &motorState);
    
    
    virtual bool setMotorPositionsRadAsync(const double *motorPositionsRad);
    
    
    virtual bool getMotorPostitonsVelocitiesForcesAndConvergencyRad(double (&motorPositionsRad)[_CFG_NUMBER_OF_MOTORS],
            double (&motorVelocitiesRadPerSec)[_CFG_NUMBER_OF_MOTORS], double (&motorForces)[_CFG_NUMBER_OF_MOTORS], bool &motorsConverge);
  
  private:
    
    const std::string _serial_port_name;
    
    LibSerial::SerialStream *_serialStreamPtr;
    
    /**
     * Returns the next (first) byte from the serial stream.
     *
     * If available the first byte of the serial stream is removed from the stream an returned as Integer type (int).
     *
     * ATTENTION: Method will never return if no byte comes available on the stream!
     *
     * @return The first byte of the serial stream as int.
     */
    int getNextByte();
    
    bool isReady();
    
    /**
     * Waits for a specific instruction flag (@see Arm7BotInstructionEnum) to be received from the serial stream ignoring any other (instruction) bytes.
     *
     * The method ignores and drops all bytes received different from the provided instruction except the start flag (@see waitForStartFlag()) as an instruction
     * is always expected to be preceeded by a start flag.
     *
     * ATTENTION: The actual instruction flag will be removed from the serial stream as well.
     *
     * @param instruction The instruction type to wait for.
     * @param timeout_sec Optional timeout in seconds. Default is "0" (no timeout).
     *
     * @return TRUE if instruction flag was received, else FALSE (in case of timeout).
     */
    bool waitForInstruction(const arm7bot::Arm7BotInstructionEnum instruction, const double timeout_sec = 0);
    
    /**
     * Waits for start flag to be received from the serial stream ignoring (dropping) any other byte.
     *
     * ATTENTION: The actual start flag will be removed from the serial stream as well.
     *
     * @see Arm7BotInstructionEnum::START_FLAG
     *
     * @param timeout_sec Optional timeout to wait for the start flag in seconds. Default is "0" (no timeout).
     *
     * @return TRUE if start flag was received, else FALSE (in case of timeout).
     */
    bool waitForStartFlag(const double timeout_sec = 0);
    
    void writeByte(const int byte);
    
    bool readFluencyAndSpeed(int *fluencyFlags, int *velocitiesDegreesPerSecond);
    
    int convertRadTo1000th(const double radian);
    
    const double convert1000thToRad(const int n1000th);
    
    bool byteAvailable() const;
    
    /**
     * Writes the motor positions, velocities, forces and the overall motion state (convergency) send by the robot to the provided variables.
     *
     * All array type variables are expected to be of size of the number of motors!
     * @see _CFG_NUMBER_OF_MOTORS;
     *
     * ATTENTION: All values are computed in radians!
     * ATTENTION: Motor velocities are not support by the robot by now. Values are set to "0" for all motors.
     *
     * @param motorPositionsRad The motor positions in radians.
     * @param motorVelocitiesRadPerSec The motor velocities in radians per second.
     * @param motorForces The motor forces at no specific unit (so far...)
     * @param motorsConverge The overall motors motion state.
     *
     * @return TRUE if motor data was read successfully, else FALSE.
     */
    bool readMotorPositionsRad(double (&motorPositionsRad)[_CFG_NUMBER_OF_MOTORS], double (&motorVelocitiesRadPerSec)[_CFG_NUMBER_OF_MOTORS],
            double (&motorForces)[_CFG_NUMBER_OF_MOTORS], bool &motorsConverge);
    
};
    
}

#endif //ROS7BOT_ARM7BOTSERIALCOMMUNICATION_H
