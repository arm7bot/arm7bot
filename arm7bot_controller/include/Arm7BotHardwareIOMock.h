//
// Created by ros on 11/4/16.
//

#ifndef ARM7BOT_ARM7BOTSERIALCOMMUNICATIONMOCK_H
#define ARM7BOT_ARM7BOTSERIALCOMMUNICATIONMOCK_H

#include <ros/ros.h>
#include "Arm7BotHardwareIOSerial.h"
#include "ArrayUtil.h"

namespace arm7bot {

class Arm7BotHardwareIOMock : public Arm7BotHardwareIO {
  
  public:
    
    Arm7BotHardwareIOMock(ros::NodeHandle &nodeHandle) {}
    
    virtual bool init() override;
    
    virtual bool getMotorPostitonsVelocitiesForcesAndConvergencyRad(double (&motorPositionsRad)[_CFG_NUMBER_OF_MOTORS],
            double (&motorVelocitiesRadPerSec)[_CFG_NUMBER_OF_MOTORS],
            double (&motorForces)[_CFG_NUMBER_OF_MOTORS],
            bool &motorsConverge);
    
    virtual bool setAllMotorState(const MotorStateEnum status);
    
    virtual bool setFluencyAndMaxSpeedDegPerSec(const int *fluencyFlags, const int *velocitiesDegreesPerSecond);
    
    virtual bool readMotorState(MotorStateEnum &motorState);
    
    bool setMotorPositionsRadAsync(const double *motorPositionsRad);
  
  
  private:
    
    std::array<float, 7> _currentMotorPositionsDeg;
    MotorStateEnum _currentMotorState;
    std::array<int, 7> _currentFluencyFlags;
    std::array<int, 7> _currentMaxVelocitiesDegreesPerSecond;
    std::array<int, 7> _currentMotorForces;
    
    std::array<float, 7> _lastMotorPositionsDeg;
    MotorStateEnum _lastMotorState;
    std::array<int, 7> _lastFluencyFlags;
    std::array<int, 7> _lastMaxVelocitiesDegreesPerSecond;
    std::array<int, 7> _lastMotorForces;
};
    
}

#endif //ARM7BOT_ARM7BOTSERIALCOMMUNICATIONMOCK_H
