//
// Created by ros on 1/12/17.
//

#include <ArrayUtil.h>

#include "Arm7BotHW.h"

arm7bot::Arm7BotHW::Arm7BotHW(ros::NodeHandle &nodeHandle) {
    
    // initialize memory
    for (size_t i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
        _jointPositionsRad[i] = 0;
        _jointVelocitiesRadPerSec[i] = 0;
        _jointEfforts[i] = 0;
        // Init position commands with the robots standard pose.
        _jointPositionsCmdRad[i] = CFG_DEG_TO_RAD_FACTOR * CFG_DEFAULT_MOTOR_POSITIONS_DEGREE[i];
    }
    _motorsConverge = false;
    
    // serial IO to hardware
    std::string param_hw_mode = nodeHandle.param("hw_mode", std::string("online"));
    if (param_hw_mode == "online") {
        _hardwareIOInterface = new Arm7BotHardwareIOSerial(nodeHandle);
    } else {
        _hardwareIOInterface = new Arm7BotHardwareIOMock(nodeHandle);
    }
    _hardwareIOInterface->init();
    
    // disable fluency and speed limit on the robots firmware.
    int noFluency[CFG_NUMBER_OF_MOTORS] = {0, 0, 0, 0, 0, 0, 0};
    int noSpeedLimit[CFG_NUMBER_OF_MOTORS] = {250, 250, 250, 250, 250, 250, 250};
    _hardwareIOInterface->setFluencyAndMaxSpeedDegPerSec(noFluency, noSpeedLimit);
    
    // setup the hardware interface
    for (int i = 0; i < CFG_NUMBER_OF_MOTORS; i++) {
        
        hardware_interface::JointStateHandle jointStateHandle(CFG_JOINT_NAMES[i], &_jointPositionsRad[i],
                &_jointVelocitiesRadPerSec[i], &_jointEfforts[i]);
        _jointStateInterface.registerHandle(jointStateHandle);
        
        hardware_interface::JointHandle jointHandle(_jointStateInterface.getHandle(CFG_JOINT_NAMES[i]),
                &_jointPositionsCmdRad[i]);
        _positionJointInterface.registerHandle(jointHandle);
        
    }
    
    registerInterface(&_jointStateInterface);
    registerInterface(&_positionJointInterface);
    
    ROS_INFO("RobotHW for 7Bot (Arm7BotHW) initialized '%s'", param_hw_mode.c_str());
}

arm7bot::Arm7BotHW::~Arm7BotHW() {
    delete _hardwareIOInterface;
}

void arm7bot::Arm7BotHW::read() {
    
    _hardwareIOInterface->getMotorPostitonsVelocitiesForcesAndConvergencyRad(_jointPositionsRad,
            _jointVelocitiesRadPerSec, _jointEfforts, _motorsConverge);
}

void arm7bot::Arm7BotHW::write() {
    
    _hardwareIOInterface->setMotorPositionsRadAsync(_jointPositionsCmdRad);
}